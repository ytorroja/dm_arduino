#define DIR_LEFT_PIN       4
#define STEP_LEFT_PIN      5
#define DIR_RIGHT_PIN      6
#define STEP_RIGHT_PIN     7
#define ENABLE_MOTORS_PIN  8
#define PEN_SERVO_PIN     A5

#define MOTOR_WIDTH                        42.0
#define MOTORS_DISTANCE_MM (500.0 - MOTOR_WIDTH)

#define HOME_X    MOTORS_DISTANCE_MM/2
#define HOME_Y                   150.0

#define GT2_MM_PER_TOOTH           2.0
#define MOTOR_STEPS_PER_TURN     200.0
#define MOTOR_MICROSTEPS_PER_STEP  2.0
#define MOTOR_TOOTHS_PER_TURN     16.0

boolean doPlot = false;

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  setupPlotter();
}

extern float curr_left_length;
extern float curr_right_length;

void loop() {
  processCommands();
  if (doPlot) {
      line(random(150, 350), random(150, 350), random(150, 350), random(150, 350));
      rect(random(150, 350), random(150, 350), random(150, 350), random(150, 350));
      ellipse(random(150, 350), random(150, 350), random(150, 350), random(150, 350));
  }
}
