extern boolean doPlot;

#define toUpper(C) (C & 0b11011111)
void processCommands() {
  if (Serial.available() > 0) {
    char cmd = Serial.read();
    
    if (toUpper(cmd) == 'P') {
      doPlot = !doPlot;
      digitalWrite(ENABLE_MOTORS_PIN, (doPlot ? HIGH : LOW));
      Serial.println((doPlot ? "Plotting..." : "Not plotting..."));
    }

    if (toUpper(cmd) == 'H') {
      digitalWrite(ENABLE_MOTORS_PIN, HIGH);
      goHome();
      digitalWrite(ENABLE_MOTORS_PIN, LOW);
    }

    if (toUpper(cmd) == 'U') {
      setPenDrawing(false);      
    }

    if (toUpper(cmd) == 'D') {
      setPenDrawing(true);
    }

    if (toUpper(cmd) == 'X') {
      float x = Serial.parseFloat();
      digitalWrite(ENABLE_MOTORS_PIN, HIGH);
      gotoXY(x, getY());
      digitalWrite(ENABLE_MOTORS_PIN, LOW);
    }

    if (toUpper(cmd) == 'Y') {
      float y = Serial.parseFloat();
      digitalWrite(ENABLE_MOTORS_PIN, HIGH);
      gotoXY(getX(), y);
      digitalWrite(ENABLE_MOTORS_PIN, LOW);
    }
    
  }
}
