#include <Servo.h> 

Servo penServo; 

#define MOTOR_STEPS_PER_MM  (MOTOR_STEPS_PER_TURN * MOTOR_MICROSTEPS_PER_STEP) / \
                            (MOTOR_TOOTHS_PER_TURN * GT2_MM_PER_TOOTH)
#define MOTOR_MM_PER_STEP   (MOTOR_TOOTHS_PER_TURN * GT2_MM_PER_TOOTH) / \
                            (MOTOR_STEPS_PER_TURN * MOTOR_MICROSTEPS_PER_STEP)

#define STEP_DELAY_MS        1
#define REVERSE_DELAY_MS   100

#define PEN_UP_DEGREES     110
#define PEN_DOWN_DEGREES    30

float curr_right_length = 0.0;
float curr_left_length  = 0.0;
int   curr_right_dir = 0;
int   curr_left_dir  = 0;
float curr_x = 0.0;
float curr_y  = 0.0;

int accum_steps_left;
int accum_steps_right;

void setupPlotter() {
  pinMode(DIR_LEFT_PIN, OUTPUT);
  pinMode(STEP_LEFT_PIN, OUTPUT);
  pinMode(DIR_RIGHT_PIN, OUTPUT);
  pinMode(STEP_RIGHT_PIN, OUTPUT);
  pinMode(ENABLE_MOTORS_PIN, OUTPUT);
  pinMode(PEN_SERVO_PIN, OUTPUT);

  penServo.attach(PEN_SERVO_PIN);  // attaches the servo on pin 9 to the servo object 
  penServo.write(PEN_UP_DEGREES);
  
  setHomeXY(HOME_X, HOME_Y);
}

void setLeftDirection(int dir) {
  if (dir < 0) {
    if (curr_left_dir >= 0) delay(REVERSE_DELAY_MS);
    digitalWrite(DIR_LEFT_PIN, HIGH);
  } else {
    if (curr_left_dir < 0) delay(REVERSE_DELAY_MS);
    digitalWrite(DIR_LEFT_PIN, LOW);
  }
  curr_left_dir = dir;
  delayMicroseconds(2);  
}

void setRightDirection(int dir) {
  if (dir < 0) {
    if (curr_right_dir >= 0) delay(REVERSE_DELAY_MS);
    digitalWrite(DIR_RIGHT_PIN, LOW); // Reverse with respect to left
  } else {
    if (curr_right_dir < 0) delay(REVERSE_DELAY_MS);
    digitalWrite(DIR_RIGHT_PIN, HIGH);
  }
  curr_right_dir = dir;
  delayMicroseconds(2);  
}

void doLeftStep(int del) {
  digitalWrite(STEP_LEFT_PIN, HIGH);
  delayMicroseconds(2);
  digitalWrite(STEP_LEFT_PIN, LOW);
  accum_steps_left += (curr_left_dir >= 0 ? 1 : -1);
  delay(del);
}

void doRightStep(int del) {
  digitalWrite(STEP_RIGHT_PIN, HIGH);
  delayMicroseconds(2);
  digitalWrite(STEP_RIGHT_PIN, LOW);
  accum_steps_right += (curr_right_dir >= 0 ? 1 : -1);
  delay(del);
}

float computeCordLength(int steps) {
  return MOTOR_MM_PER_STEP * steps;
}

int computeLeftSteps(float l) {
  float leftDiff = l - curr_left_length;
  return MOTOR_STEPS_PER_MM * leftDiff;
}

int computeRightSteps(float r) {
  float rightDiff = r - curr_right_length;
  return MOTOR_STEPS_PER_MM * rightDiff;
}

void moveSequentialTo(float l_length, float r_length, int del) {
  int lsteps = computeLeftSteps(l_length);
  setLeftDirection(lsteps);
  lsteps = abs(lsteps);
  for(int i = 0; i < lsteps; i++) doLeftStep(del);
  curr_left_length = l_length;

  int rsteps = computeRightSteps(r_length);
  setRightDirection(rsteps);
  rsteps = abs(rsteps);
  for(int i = 0; i < rsteps; i++) doRightStep(del);
  curr_right_length = r_length;
}

void moveParallelTo(float l_length, float r_length, int del) {
  int ls = round(computeLeftSteps(l_length));
  int rs = round(computeRightSteps(r_length));
  
//  Serial.print("move ");
//  Serial.print(ls);
//  Serial.print(",");
//  Serial.println(rs);
  
  curr_left_length  += computeCordLength(ls);
  curr_right_length += computeCordLength(rs);
  
  setLeftDirection(ls);
  setRightDirection(rs);
  ls = abs(ls);
  rs = abs(rs);
  if (ls > rs) {
    float incr   = ((float)rs)/ls;
    float incr_f = 0;
    int   incr_i = 0;
    int   incl_i = 0;
    for(int i = 0; i < ls; i++) {
      doLeftStep(del);
      incl_i++;
      if (((int)incr_f) > incr_i) {
        doRightStep(del);
        incr_i++; //   = ((int)incr_f);
      }
      incr_f += incr;
    }
    if (incr_i < rs) {
      if (rs - incr_i >= 2) {
        Serial.print("Diferencia en Right:");
        Serial.println(rs - incr_i);
      }
      for(int i = 0; i < rs - incr_i; i++) {
        doRightStep(del);
        incr_i++;
      }
    }
    if (ls != incl_i) Serial.print("Err L (L>R)");
    if (rs != incr_i) Serial.print("Err R (L>R)");
  } else {
    float incl = ((float)ls)/rs;
    float incl_f = 0;
    int   incl_i = 0;
    int   incr_i = 0;
    for(int i = 0; i < rs; i++) {
      doRightStep(del);
      incr_i++;
      if (((int)incl_f) > incl_i) {
        doLeftStep(del);
        incl_i++; //  = ((int)incl_f);
      }
      incl_f += incl;
    }
    if (incl_i < ls) {
      if (ls - incl_i >= 2) {
        Serial.print("Diferencia en Left:");
        Serial.println(ls - incl_i);
      }
      for(int i = 0; i < ls - incl_i; i++) {
        doLeftStep(del);
        incl_i++;
      }
    }
    if (ls != incl_i) Serial.print("Err L (L<R)");
    if (rs != incr_i) Serial.print("Err R (L<R)");
  } 
}
  
void gotoXY(float x, float y) {
  float left_length  = sqrt(pow(x, 2)  + pow(y, 2));
  float xr = MOTORS_DISTANCE_MM - x;
  float right_length = sqrt(pow(xr, 2) + pow(y, 2));
  // moveSequentialTo(left_length, right_length, STEP_DELAY_MS);
  moveParallelTo(left_length, right_length, STEP_DELAY_MS);
  curr_x = x;
  curr_y = y;
}

void setPenDrawing(boolean pen) {
  if (pen) { // Drawing
    penServo.write(PEN_DOWN_DEGREES);
  } else { // Not drawing
    penServo.write(PEN_UP_DEGREES);    
  }
  delay(500); // Time for the pen to stay quiet
}

float getX() {
  return curr_x;
}

float getY() {
  return curr_y;
}

void lineTo(float x2, float y2) {
  float x1 = getX();
  float y1 = getY();
  float dist = sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));
  float inc  = 5.0 / dist; // Min segment 5mm
  gotoXY(x1, y1);
  for(float t = inc; t < 1.0; t += inc) {
    float nx = (1.0 - t) * x1 + t * x2;
    float ny = (1.0 - t) * y1 + t * y2;
    gotoXY(nx, ny);     
  }
  gotoXY(x2, y2);
}

void line(float x1, float y1, float x2, float y2) {
  float dist = sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));
  float inc  = 5.0 / dist; // Min segment 5mm
  setPenDrawing(false);
  gotoXY(x1, y1);
  setPenDrawing(true);
  for(float t = inc; t < 1.0; t += inc) {
    float nx = (1.0 - t) * x1 + t * x2;
    float ny = (1.0 - t) * y1 + t * y2;
    gotoXY(nx, ny);     
  }
  gotoXY(x2, y2);
  setPenDrawing(false);
}

void rect(float x1, float y1, float x2, float y2) {
  setPenDrawing(false);
  gotoXY(x1, y1); 
  setPenDrawing(true);
  lineTo(x1, y2);
  lineTo(x2, y2);
  lineTo(x2, y1);
  lineTo(x1, y1);
  setPenDrawing(false);
}

void ellipse(float x1, float y1, float x2, float y2) {
  float rx = (x2 - x1)/2;
  float ry = (y2 - y1)/2;
  float cx = (x2 + x1)/2;
  float cy = (y2 + y1)/2;
  float perim = 2 * PI * sqrt((pow(rx, 2) + pow(ry, 2))/2); // aprox
  float inc   = 5.0 / perim; // Min segment 5mm
  setPenDrawing(false);
  gotoXY(cx + rx, cy);
  setPenDrawing(true);
  for(float t = inc; t < 1.0; t += inc) {
    float angle = 2 * PI * t;
    float nx = cx + rx * cos(angle);
    float ny = cy + ry * sin(angle);
    lineTo(nx, ny);     
  }  
  lineTo(cx + rx, cy);
  setPenDrawing(false);
}

void goHome() {
  setPenDrawing(false);
  gotoXY(HOME_X, HOME_Y);
}

void setHomeXY(float x, float y) {
  curr_left_length  = sqrt(pow(x, 2) + pow(y, 2));
  float xr = MOTORS_DISTANCE_MM - x;
  curr_right_length = sqrt(pow(xr, 2) + pow(y, 2));
  curr_x = x;
  curr_y = y;
}

