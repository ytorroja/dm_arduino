///////////////////////////////////////////////////////////////////////////////
// Harmonograph with continous rotation sevos
// In the terminal, introduce lxxxx or rxxxx, where xxxx is the servo speed
// 0 means stopped, 500 means full speed clockwise, -500 full counterclockwise
// CC-SA Yago Torroja 2015
///////////////////////////////////////////////////////////////////////////////
#include <Servo.h> 

#define LEFT_SERVO_PIN    9
#define RIGHT_SERVO_PIN  10

Servo myservo_l;  // create servo object to control the left servo 
Servo myservo_r;  // create servo object to control the right servo 
 
void setup() 
{ 
  Serial.begin(9600);
  myservo_l.attach(LEFT_SERVO_PIN);  // attaches the servo on pin 9 to the servo object 
  myservo_r.attach(RIGHT_SERVO_PIN);  // attaches the servo on pin 9 to the servo object 
  myservo_l.writeMicroseconds(1500);
  myservo_r.writeMicroseconds(1500);
} 


void loop() 
{ 
  processCommands();
} 

