#define toUpper(C) (C & 0b11011111)
void processCommands() {
  if (Serial.available() > 0) {
    char command = Serial.read();
    Serial.print((char)toUpper(command));
    
    if (toUpper(command) == 'L') { // Controls Left Servo
      int uSeconds = Serial.parseInt() + 1500;
      uSeconds = constrain(uSeconds, 1000, 2000);
      myservo_l.writeMicroseconds(uSeconds);
      l_us_center = uSeconds;
    }    
    
    if (toUpper(command) == 'R') { // Controls Right Servo
      int uSeconds = Serial.parseInt() + 1500;
      uSeconds = constrain(uSeconds, 1000, 2000);
      myservo_r.writeMicroseconds(uSeconds);
    }    
    
  }
}
