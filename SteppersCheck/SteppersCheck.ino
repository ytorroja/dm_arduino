#define DIR_LEFT    4
#define STEP_LEFT   5
#define DIR_RIGHT   6
#define STEP_RIGHT  7

void setup() {
  // put your setup code here, to run once:
  pinMode(DIR_LEFT, OUTPUT);
  pinMode(STEP_LEFT, OUTPUT);
  pinMode(DIR_RIGHT, OUTPUT);
  pinMode(STEP_RIGHT, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(DIR_LEFT, !digitalRead(DIR_LEFT));
  digitalWrite(DIR_RIGHT, !digitalRead(DIR_RIGHT));
    delayMicroseconds(2);
  for(int i = 0; i < 1000; i++) {
    digitalWrite(STEP_LEFT, HIGH);
    digitalWrite(STEP_RIGHT, HIGH);
    delayMicroseconds(2);
    digitalWrite(STEP_LEFT, LOW);
    digitalWrite(STEP_RIGHT, LOW);
    delay(1);
  }
  delay(500);
}
