///////////////////////////////////////////////////////////////////////////////
// Servo calibration utility
// Calibration of servo positions with Servo library
// In the terminal, introduce dxxx, where xxx is the servo position in degrees
// Normal servos allow moving from 0º (totally left) to 180º (totally right)
// 90º is normally centered position
// CC-SA Yago Torroja 2015
///////////////////////////////////////////////////////////////////////////////
#include <Servo.h> 

#define SERVO_PIN  9

Servo myservo;  

void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(SERVO_PIN);  // attaches the servo object to the pin 
  myservo.writeMicroseconds(1500);
} 

#define toUpper(C) (C & 0b11011111)
void loop() 
{ 
  if (Serial.available() > 0) {
    char command = Serial.read(); 
    Serial.println((char)toUpper(command));
    if (toUpper(command) == 'D') { // To lower
      int deg = Serial.parseInt();
      myservo.write(deg);
    }
  }
  
} 

