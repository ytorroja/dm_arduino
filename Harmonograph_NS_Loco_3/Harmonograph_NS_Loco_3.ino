#include <Servo.h> 
 
Servo myservo_l;  // create servo object to control a servo 
Servo myservo_r;  // create servo object to control a servo 
 
void setup() 
{ 
  Serial.begin(9600);
  myservo_l.attach( 9);  // attaches the servo on pin 9 to the servo object 
  myservo_r.attach(11);  // attaches the servo on pin 9 to the servo object 
  myservo_l.writeMicroseconds(1500);
  myservo_r.writeMicroseconds(1500);
} 

int left_ref  = 1400;
int right_ref = 1600;

int left_center  = 1600;
int right_center = 1400;
int right_center_center = 1400;

int left_amp  = 250;
int right_amp = 150;
int right_amp_center = 150;

float left_speed  = 0.002;
float right_speed = 0.17;

float left_angle  = 0.0;
float right_angle = 0.0;

float right_speed_center = 0.17;

float time = 0;
float time_inc = 0.5;

boolean doPaint = false;

void loop() 
{ 
  processCommands();
  
  if (doPaint) {
    left_angle += left_speed * time_inc;
    right_angle += right_speed * time_inc;
    
    left_ref = left_center + left_amp * (sin(left_angle) + cos(right_angle));
    right_ref = right_center + right_amp * (sin(right_angle) + cos(left_angle));

    //right_speed = right_speed_center + 0.3 * sin(time/100);
    right_amp = right_amp_center + 100 * cos(time/1000);
    right_center = right_center_center + 100 * sin(time/1000);
    Serial.println(right_speed*1000);
   
    myservo_l.writeMicroseconds(left_ref);
    myservo_r.writeMicroseconds(right_ref);
  
    time = time + time_inc;
   
    delay(10);
  }
} 

